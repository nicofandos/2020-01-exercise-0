package exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

    @Test
    public void testItemName() {
        Item item = new Item(Constants.NORMAL_ITEM_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        assertThat(Constants.NORMAL_ITEM_NAME).isEqualTo(item.Name);
    }

    @Test
    public void testItemInitialSellInDate() {
        Item item = new Item(Constants.NORMAL_ITEM_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        assertThat(Constants.NORMAL_ITEM_INITIAL_SELL_IN).isEqualTo(item.sellIn);
    }

    @Test
    public void testItemInitialQuality() {
        Item item = new Item(Constants.NORMAL_ITEM_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        assertThat(Constants.NORMAL_ITEM_INITIAL_QUALITY).isEqualTo(item.quality);
    }

    @Test
    public void testNormalItemDegradation() {
        Item item = new Item(Constants.NORMAL_ITEM_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        app.updateQuality();
        assertThat(Constants.NORMAL_ITEM_INITIAL_QUALITY - Constants.NORMAL_ITEM_DEGRADATION).isEqualTo(item.quality);
    }

    @Test
    public void testNormalItemDegradationAfterSellDate() {
        Item item = new Item(Constants.NORMAL_ITEM_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        for(int days = 0; days < Constants.NORMAL_ITEM_INITIAL_SELL_IN; ++days) {
            app.updateQuality();
        }
        int expectedQuality = Constants.NORMAL_ITEM_INITIAL_QUALITY - (Constants.NORMAL_ITEM_INITIAL_SELL_IN * Constants.NORMAL_ITEM_DEGRADATION);
        assertThat(expectedQuality).isEqualTo(item.quality);
        app.updateQuality();
        expectedQuality -= 2 * Constants.NORMAL_ITEM_DEGRADATION;
        assertThat(expectedQuality).isEqualTo(item.quality);
    }

    @Test
    public void testItemQualityIsNeverNegative() {
        Item item = new Item(Constants.NORMAL_ITEM_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        for(int days = 0; days < 2*Constants.NORMAL_ITEM_INITIAL_QUALITY; ++days) {
            app.updateQuality();
        }
        assertThat(Constants.MIN_ITEM_QUALITY).isEqualTo(item.quality);
    }

    @Test
    public void testNormalItemQualityIsNotBiggerThan50() {
        Item item = new Item(Constants.NORMAL_ITEM_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.SULFURAS_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        app.updateQuality();
        assertThat(Constants.MAX_ITEM_QUALITY).isGreaterThan(item.quality);
    }

    @Test
    public void testAgedBrieIncreasesQuality() {
        Item item = new Item(Constants.AGED_BRIE_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        app.updateQuality();
        assertThat(Constants.NORMAL_ITEM_INITIAL_QUALITY + Constants.AGED_ITEM_DEGRADATION).isEqualTo(item.quality);
    }

    @Test
    public void testAgedBrieIncreasesQualityAfterSellDate() {
        Item item = new Item(Constants.AGED_BRIE_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        for(int days = 0; days < Constants.NORMAL_ITEM_INITIAL_SELL_IN; ++days) {
            app.updateQuality();
        }
        int expectedQuality = Constants.NORMAL_ITEM_INITIAL_QUALITY + (Constants.NORMAL_ITEM_INITIAL_SELL_IN * Constants.AGED_ITEM_DEGRADATION);
        assertThat(expectedQuality).isEqualTo(item.quality);
        app.updateQuality();
        expectedQuality += 2 * Constants.AGED_ITEM_DEGRADATION;
        assertThat(expectedQuality).isEqualTo(item.quality);
    }

    @Test
    public void testAgedItemQualityIsNeverOver50() {
        Item item = new Item(Constants.AGED_BRIE_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        for(int days = 0; days < 2*Constants.NORMAL_ITEM_INITIAL_QUALITY; ++days) {
            app.updateQuality();
        }
        assertThat(Constants.MAX_ITEM_QUALITY).isEqualTo(item.quality);
    }

    @Test
    public void testAgedItemQualityIsNotNegative() {
        Item item = new Item(Constants.AGED_BRIE_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             -1 * Constants.SULFURAS_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        app.updateQuality();
        assertThat(Constants.MIN_ITEM_QUALITY).isLessThan(item.quality);
    }

    @Test
    public void testSulfurasSellInDontChange() {
        Item item = new Item(Constants.SULFURAS_NAME,
                             Constants.SULFURAS_INITIAL_SELL_IN,
                             Constants.SULFURAS_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        assertThat(Constants.SULFURAS_INITIAL_SELL_IN).isEqualTo(item.sellIn);
        for(int days = 0; days < Constants.NORMAL_ITEM_INITIAL_SELL_IN; ++days) {
            app.updateQuality();
        }
        assertThat(Constants.SULFURAS_INITIAL_SELL_IN).isEqualTo(item.sellIn);
    }
    
    @Test
    public void testSulfurasQualityDontChange() {
        Item item = new Item(Constants.SULFURAS_NAME,
                             Constants.SULFURAS_INITIAL_SELL_IN,
                             Constants.SULFURAS_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        assertThat(Constants.SULFURAS_INITIAL_QUALITY).isEqualTo(item.quality);
        for(int days = 0; days < Constants.NORMAL_ITEM_INITIAL_SELL_IN; ++days) {
            app.updateQuality();
        }
        assertThat(Constants.SULFURAS_INITIAL_QUALITY).isEqualTo(item.quality);
    }

    @Test
    public void testBackstagePassesQualityIncreasesOverTime() {
        Item item = new Item(Constants.BACKSTAGE_PASSES_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.MIN_ITEM_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        
        for(int days = Constants.NORMAL_ITEM_INITIAL_SELL_IN; days > 10; --days) {
            app.updateQuality();
        }
        int expectedQuality = Constants.MIN_ITEM_QUALITY + ((Constants.NORMAL_ITEM_INITIAL_SELL_IN - 10) * Constants.AGED_ITEM_DEGRADATION);
        assertThat(expectedQuality).isEqualTo(item.quality);
        
        for(int days = 10; days > 5; --days) {
            app.updateQuality();
        }
        expectedQuality += 5 * (2 * Constants.AGED_ITEM_DEGRADATION);
        assertThat(expectedQuality).isEqualTo(item.quality);

        for(int days = 5; days >= 0; --days) {
            app.updateQuality();
        }
        expectedQuality += 6 * (3 * Constants.AGED_ITEM_DEGRADATION);
        assertThat(expectedQuality).isEqualTo(item.quality);

    }

    @Test
    public void testBackstagePassesQualityDropsToZeroAfterSellDate() {
        Item item = new Item(Constants.BACKSTAGE_PASSES_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        for(int days = Constants.NORMAL_ITEM_INITIAL_SELL_IN; days >= -1; --days) {
            app.updateQuality();
        }
        assertThat(Constants.MIN_ITEM_QUALITY).isEqualTo(item.quality);
    }

    @Test
    public void testConjuredItemDegradation() {
        Item item = new Item(Constants.CONJURED_ITEM_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.NORMAL_ITEM_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        app.updateQuality();
        assertThat(Constants.NORMAL_ITEM_INITIAL_QUALITY - (2*Constants.NORMAL_ITEM_DEGRADATION)).isEqualTo(item.quality);
    }

    @Test
    public void testConjuredItemDegradationAfterSellDate() {
        Item item = new Item(Constants.CONJURED_ITEM_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.MAX_ITEM_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        for(int days = 0; days < Constants.NORMAL_ITEM_INITIAL_SELL_IN; ++days) {
            app.updateQuality();
        }
        int expectedQuality = Constants.MAX_ITEM_QUALITY - 2 * (Constants.NORMAL_ITEM_INITIAL_SELL_IN * Constants.NORMAL_ITEM_DEGRADATION);
        assertThat(expectedQuality).isEqualTo(item.quality);
        app.updateQuality();
        expectedQuality -= 2 * 2 * Constants.NORMAL_ITEM_DEGRADATION;
        assertThat(expectedQuality).isEqualTo(item.quality);
    }

    @Test
    public void testConjuredItemQualityIsNotBiggerThan50() {
        Item item = new Item(Constants.CONJURED_ITEM_NAME,
                             Constants.NORMAL_ITEM_INITIAL_SELL_IN,
                             Constants.SULFURAS_INITIAL_QUALITY);
        GildedRose app = new GildedRose(new Item[] {item});
        app.updateQuality();
        assertThat(Constants.MAX_ITEM_QUALITY).isGreaterThan(item.quality);
    }

}
