package exercise;

import java.util.TreeMap;

public final class Constants {
    public static final int NORMAL_ITEM_DEGRADATION = 1;
    public static final int AGED_ITEM_DEGRADATION = 1;
    public static final int MIN_ITEM_QUALITY = 0;
    public static final int MAX_ITEM_QUALITY = 50;
    public static final int SULFURAS_QUALITY = 80;
    public static final int BACKSTAGE_QUALITY_INCREASE = 2;
    public static final int BACKSTAGE_QUALITY_INCREASE_CLOSE_TO_CONCERT = 3;

    public static final String AGED_BRIE_NAME = "Aged Brie";
    public static final String BACKSTAGE_PASSES_NAME = "Backstage Passes";
    public static final String SULFURAS_NAME = "Sulfuras";
    public static final String NORMAL_ITEM_NAME = "Normal Item";
    public static final String CONJURED_ITEM_NAME = "Conjured";
    public static final int NORMAL_ITEM_INITIAL_SELL_IN = 15;
    public static final int NORMAL_ITEM_INITIAL_QUALITY = 25;
    public static final int SULFURAS_INITIAL_SELL_IN = Integer.MAX_VALUE;
    public static final int SULFURAS_INITIAL_QUALITY = 80;

    //Treemap: <MaxDaysUntilConcert, increaseInQuality>
    public static final TreeMap<Integer, Integer> BACKSTAGE_ITEM_QUALITY_MAP;
    static {
        BACKSTAGE_ITEM_QUALITY_MAP = new TreeMap<>();
        BACKSTAGE_ITEM_QUALITY_MAP.put(-1, -MAX_ITEM_QUALITY);
        BACKSTAGE_ITEM_QUALITY_MAP.put(5, BACKSTAGE_QUALITY_INCREASE_CLOSE_TO_CONCERT);
        BACKSTAGE_ITEM_QUALITY_MAP.put(10, BACKSTAGE_QUALITY_INCREASE);
        BACKSTAGE_ITEM_QUALITY_MAP.put(Integer.MAX_VALUE, AGED_ITEM_DEGRADATION);
    }


}
