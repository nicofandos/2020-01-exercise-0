package exercise;

public class ItemUpdater {

    public static void sulfurasItemUpdate(Item item) {
        item.sellIn = Integer.MAX_VALUE;
        item.quality = Constants.SULFURAS_QUALITY;
    }

    public static void decreaseNormalItemQuality(Item item) {
        item.quality = Math.max(Constants.MIN_ITEM_QUALITY, item.quality - Constants.NORMAL_ITEM_DEGRADATION);
    }

    public static void normalItemUpdate(Item item) {
        item.quality = Math.min(Constants.MAX_ITEM_QUALITY, item.quality);
        if (item.sellIn <= 0) {
            decreaseNormalItemQuality(item);
        }
        decreaseNormalItemQuality(item);
        item.sellIn -= 1;
    }

    public static void increaseAgedItemQuality(Item item) {
        item.quality = Math.min(Constants.MAX_ITEM_QUALITY, item.quality + Constants.AGED_ITEM_DEGRADATION);
    }

    public static void agedBrieItemUpdate(Item item) {
        item.quality = Math.max(Constants.MIN_ITEM_QUALITY, item.quality);
        if (item.sellIn <= 0) {
            increaseAgedItemQuality(item);
        }
        increaseAgedItemQuality(item);
        item.sellIn -= 1;
    }

    public static void updateBackstageItemQuality(Item item) {
        item.quality += Constants.BACKSTAGE_ITEM_QUALITY_MAP.ceilingEntry(item.sellIn).getValue();
        item.quality = Math.min(Constants.MAX_ITEM_QUALITY, item.quality);
        item.quality = Math.max(Constants.MIN_ITEM_QUALITY, item.quality);
    }

    public static void backstagePassesItemUpdate(Item item) {
        updateBackstageItemQuality(item);
        item.sellIn -= 1;
    }

    public static void decreaseConjuredItemQuality(Item item) {
        decreaseNormalItemQuality(item);
        decreaseNormalItemQuality(item);
    }

    public static void conjuredItemUpdate(Item item) {
        item.quality = Math.min(Constants.MAX_ITEM_QUALITY, item.quality);
        if (item.sellIn <= 0) {
            decreaseConjuredItemQuality(item);
        }
        decreaseConjuredItemQuality(item);
        item.sellIn -= 1;
    }

}
