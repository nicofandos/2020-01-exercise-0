package exercise;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;
import java.util.function.Consumer;


class GildedRose {
    Item[] items;
    private HashMap<String, Consumer<Item>> updateMap;

    public GildedRose(Item[] _items) {
        this.items = _items;
        // All the items that are not specified are considered normal items
        this.updateMap = new HashMap<>();
        this.updateMap.put(Constants.SULFURAS_NAME, ItemUpdater::sulfurasItemUpdate);
        this.updateMap.put(Constants.AGED_BRIE_NAME, ItemUpdater::agedBrieItemUpdate);
        this.updateMap.put(Constants.BACKSTAGE_PASSES_NAME, ItemUpdater::backstagePassesItemUpdate);
        this.updateMap.put(Constants.CONJURED_ITEM_NAME, ItemUpdater::conjuredItemUpdate);
    }

    private void updateItemQuality(Item item) {
        this.updateMap.getOrDefault(
            item.Name, ItemUpdater::normalItemUpdate
            ).accept(item);
    }

    public void updateQuality() {
        for (Item item: this.items) {
            this.updateItemQuality(item);
        }
    }

}
